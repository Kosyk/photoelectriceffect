package pl.edu.pw.fizyka.pojava.GiedykKosyl;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Listener pozwalający na wpisywanie tylko cyfer i kropek (potrzebnych do double)
 * @author Katarzyna Kosyl
 */
public class InputListener extends KeyAdapter implements KeyListener{
	public void keyTyped(KeyEvent e){
		char c = e.getKeyChar();
		if (!((c >= '0') && (c <= '9') ||
			(c == KeyEvent.VK_BACK_SPACE) ||
			(c == KeyEvent.VK_DELETE) ||
			 c == '.')) {
			      e.consume();
		}
	}
}
