package pl.edu.pw.fizyka.pojava.GiedykKosyl;

/**
 * 
 * @author Katarzyna Giedyk
 *
 */
//Ta klasa jest rodzajem instrukcji dla drugiego wątku
public class PhotoelectricRunnable implements Runnable { 

	Interface refInterface; //referencja do interfejsu

	public PhotoelectricRunnable(Interface in) {
		refInterface = in;
	}

	public void run() { //Co ma się dziać w drugim watku.
		while (true) { //wieczna pętla
			if (refInterface.ifSimulate) {//kiedy symulacja jest włączona
				for (int z=0;z<5;z++) //5 obliczeń, przed repaint
				for (int ii = 0; ii < refInterface.photoelectricCalculator.particleAmount; ii++) {
					//Pętla leci po wszystkich kulkach.
					refInterface.photoelectricCalculator.setDistance(ii);	
				}
				try {
					refInterface.repaint();
					Thread.sleep(20); //usypianie drugiego wątku.
				} catch (InterruptedException e) {//Łapanie wyjątku
					e.printStackTrace();  //wypisywanie informacji o wyjątku na tablice.
					
				}
			}
		}
	}

}
