package pl.edu.pw.fizyka.pojava.GiedykKosyl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartPanel;

import net.miginfocom.swing.MigLayout;

/**
 * Klasa stanowiąca interfejs programu. 
 * Umożliwia symulację efetu fotoelektrycznego.
 */
@SuppressWarnings("serial")
public class Interface extends JPanel {
	
	volatile boolean ifSimulate;
	
	double voltage;
	String  stringToErrorMessage[]=new String[2];
	JRadioButton knownWorkFunction;
	JRadioButton undefinedWorkFunction;
	JSlider voltageSlider;
	JTextField undefinedWorkFunctionField; 
	JTextField wavelengthField;
	JTextField voltageField;
	JButton simulationTrigger, saveGraph, chooseEnglish, choosePolish;
	JButton colorPink, colorBlue, colorGray, colorGreen, colorYellow;
	JComboBox<String> metalList;
	Graph graph;
	PhotoelectricCalculator photoelectricCalculator;
	Thread backgroundThread;	

	public Interface() {
		//Ustawienia ogólne
		setLayout(new BorderLayout());
		setMinimumSize(new Dimension(840, 600));

		final Interface mInterface=this;
		setLF(); 
		backgroundThread = new Thread(new PhotoelectricRunnable(this));
		
		//Panele
		final JPanel panelWithGraph = new JPanel();
		panelWithGraph.setBackground(Color.GRAY);
		graph=new Graph(this); // referencja w grafie do interfejsu
		graph.setVisible(true); // widooczność
		
		final JPanel panelWithParametres = new JPanel();
		final JPanel parametres = new JPanel (new MigLayout("center"));
		
		//Praca wyjścia - 2 scenariusze
		ButtonGroup chooseWorkFunction = new ButtonGroup();
		knownWorkFunction = new JRadioButton("Lista metali");
		undefinedWorkFunction = new JRadioButton("Zadana przez użytkownika  (0.1 - 6 eV)");
		undefinedWorkFunction.setSelected(true);
		chooseWorkFunction.add(knownWorkFunction);
		chooseWorkFunction.add(undefinedWorkFunction);	
		
		metalList = new JComboBox<String>();
		metalList.addItem("Cez [2,14]");
		metalList.addItem("Sód [2,75]");
		metalList.addItem("Aluminium [4,08]");
		metalList.addItem("Żelazo [4,5]");
		metalList.addItem("Srebro [4,73]");
		metalList.addItem("Nikiel [5,01]");
		metalList.addItem("Złoto [5,1]");
		metalList.setEnabled(false);
		
		final JLabel workLabel = new JLabel("Praca wyjścia [eV]");
		undefinedWorkFunctionField = new JTextField("2");
		undefinedWorkFunctionField.setEnabled(true);
		undefinedWorkFunctionField.addKeyListener(new InputListener());
		wavelengthField = new JTextField("300");
		wavelengthField.addKeyListener(new InputListener());
		voltageField = new JTextField("0.0");
		voltageField.setEditable(false);
		voltageField.setBackground(Color.WHITE);
		final JLabel wavelengthLabel = new JLabel("Długość fali λ[nm] (100 - 800 nm)");
		final JLabel voltageLabel = new JLabel("Napięcie [V]");
		final JLabel colorsLabel = new JLabel("Kolory");
		
		stringToErrorMessage[0]="Liczba jest poza zakresem!";
		stringToErrorMessage[1]="W parametry należy wpisać liczbę!";
		
		// Powstanie kalkulatora dla wszystkich obliczeń
		photoelectricCalculator = new PhotoelectricCalculator(this);
		
		voltageSlider = new JSlider();
		voltageSlider.setMinimum(-50);
		voltageSlider.setMaximum(50);
		voltageSlider.setValue(0);
		voltageSlider.addChangeListener(new ChangeListener(){// reakcja na ruch slidera
			public void stateChanged(ChangeEvent e) {
				 voltage=voltageSlider.getValue();//napiecie przyjmuje wartość ze slidera
				voltageField.setText(Double.toString(voltage/10));
				photoelectricCalculator.acceleration= photoelectricCalculator.setAcceleration(voltage/10);
				//ustawianie przyspieszenia na podstawie napięcia
				if (ifSimulate == true){		
					graph.calculateCurrent(voltage/10);
				}
				
			}
		}
		);
			
		colorGray = new JButton();
		colorPink = new JButton();
		colorBlue = new JButton();
		colorYellow = new JButton();
		colorGreen = new JButton();		
		colorGray.setBackground(Color.GRAY);
		colorPink.setBackground(Color.PINK);
		colorBlue.setBackground(Color.BLUE);
		colorYellow.setBackground(Color.YELLOW);
		colorGreen.setBackground(Color.GREEN);
		
		simulationTrigger = new JButton("Start");
		saveGraph = new JButton("Zapisz");	
		saveGraph.addActionListener(new SavingListener(this));
		
		//listenery wyboru metali
		knownWorkFunction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				metalList.setEnabled(true);
				undefinedWorkFunctionField.setEnabled(false);				
			}
		});
		undefinedWorkFunction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				metalList.setEnabled(false);
				undefinedWorkFunctionField.setEnabled(true);				
			}
		});
		
		//ustawianie języka
		final JLabel languageLabel = new JLabel("Wybierz Język:");
		chooseEnglish = new JButton("English"); 
		choosePolish = new JButton("Polski");
		chooseEnglish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "English Chosen", "", 1);
				workLabel.setText("Work Function [eV]");
				knownWorkFunction.setText("Metal list");
				undefinedWorkFunction.setText("User choice  (0.1 - 6 eV)");
				wavelengthLabel.setText("Wave length λ[nm] (100 - 800 nm)");
				voltageLabel.setText("Voltage [V]");
				languageLabel.setText("Choose language");
				saveGraph.setText("Save");
				colorsLabel.setText("Colors");

				metalList.removeAllItems(); //usuwa elementy z lisy
				metalList.addItem("Cesium [2,14]");
				metalList.addItem("Natrium [2,75]");
				metalList.addItem("Aluminum [4,08]");
				metalList.addItem( "Iron [4,5]");
				metalList.addItem("Silver [4,73]");
				metalList.addItem("Nickel [5,01]");
				metalList.addItem("Gold [5,1]");

				parametres.setBorder(BorderFactory.createTitledBorder("Parameters"));
				stringToErrorMessage[0]="Number out of intervall!";
				stringToErrorMessage[1]="Input string must be a number!";		
			}
		});
		choosePolish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null,"Wybrano Polski", "", 1);
				workLabel.setText("Praca wyjścia [eV]");
				knownWorkFunction.setText("Lista metali");
				undefinedWorkFunction.setText("Zadana przez użytkownika  (0.1 - 6 eV)");
				wavelengthLabel.setText("Długość fali λ[nm] (100 - 800 nm)");
				voltageLabel.setText("Napięcie [V]");
				languageLabel.setText("Wybierz Język");
				saveGraph.setText("Zapisz");
				colorsLabel.setText("Kolory");
				metalList.removeAllItems();//usuwa elementy z listy
				metalList.addItem("Cez [2,14]");
				metalList.addItem("Sód [2,75]");
				metalList.addItem("Aluminium [4,08]");
				metalList.addItem("Żelazo [4,5]");
				metalList.addItem("Srebro [4,73]");
				metalList.addItem("Nikiel [5,01]");
				metalList.addItem("Złoto [5,1]");
				parametres.setBorder(BorderFactory.createTitledBorder("Parametry"));
				stringToErrorMessage[0]="Liczba jest poza zakresem!";
				stringToErrorMessage[1]="W parametry należy wpisać liczbę!";
			}
		}); 
		
		//listener dla przycisku start/stop	
		simulationTrigger.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				if(ifSimulate == true){
					ifSimulate = false;
					simulationTrigger.setText("Start");
					photoelectricCalculator.simulationTimer.stop();
					for (int ii = 0; ii < photoelectricCalculator.particleAmount; ii++) {
						photoelectricCalculator.distance[ii] = -1;
					}
					repaint();
					wavelengthField.setEnabled(true);
					knownWorkFunction.setEnabled(true);
					undefinedWorkFunction.setEnabled(true);
					if (knownWorkFunction.isSelected())
						metalList.setEnabled(true);
					if (undefinedWorkFunction.isSelected())
						undefinedWorkFunctionField.setEnabled(true);
				}
				else{
					if(checkIntervall(wavelengthField, 100, 800) && checkIntervall(undefinedWorkFunctionField, 0.1, 6)){
						ifSimulate = true;
						simulationTrigger.setText("Stop");
						photoelectricCalculator.setParametersFromInterface(mInterface);
						photoelectricCalculator.setInitialVelocity();
						photoelectricCalculator.acceleration= photoelectricCalculator.setAcceleration(voltage/10);
						for (int ii = 0; ii < photoelectricCalculator.particleAmount; ii++)//pętla po wszyskich kulkach
							photoelectricCalculator.velocity[ii]=photoelectricCalculator.initialVelocity;
						graph.series.clear();
						wavelengthField.setEnabled(false);
						knownWorkFunction.setEnabled(false);
						metalList.setEnabled(false);
						undefinedWorkFunction.setEnabled(false);
						undefinedWorkFunctionField.setEnabled(false);
						photoelectricCalculator.simulationTimer.start();
					}
				}								
			}			
			}
		);
		
		//Doadawanie wszystkiege, ustawianie rozmieszczenia		
		panelWithParametres.setLayout(new MigLayout("center"));
		parametres.add(workLabel, "wrap, gaptop 10"); 
		parametres.add(knownWorkFunction, "wrap");
		parametres.add(metalList, "wrap");
		parametres.add(undefinedWorkFunction, "wrap");
		parametres.add(undefinedWorkFunctionField, "w 50!, wrap");
		parametres.add(wavelengthLabel, "wrap, gaptop 20");
		parametres.add(wavelengthField, "w 50!, wrap");
		parametres.add(voltageLabel, "wrap, gaptop 20");
		parametres.add(voltageField, "w 50!, wrap");
		parametres.add(voltageSlider, "wrap, gaptop 20");
		parametres.setBorder(BorderFactory.createTitledBorder("Parametry"));
		
		panelWithParametres.add(parametres, "wrap, wrap");
		panelWithParametres.add(languageLabel, "wrap, gaptop 20"); 
		panelWithParametres.add(chooseEnglish, "w 100!, wrap, gaptop 10");
		panelWithParametres.add(choosePolish, "w 100!, wrap, gaptop 10");
		
		panelWithParametres.add(colorsLabel,"wrap, gaptop 70");
		panelWithParametres.add(colorGray, "w 20!, h 20! ,split 5");
		panelWithParametres.add(colorPink,"w 20!, h 20!");
		panelWithParametres.add(colorBlue,"w 20!, h 20!");
		panelWithParametres.add(colorYellow,"w 20!, h 20!");
		panelWithParametres.add(colorGreen,"w 20!, h 20!");
			
		final JPanel controlPanel = new JPanel(new MigLayout("right"));
		controlPanel.add(simulationTrigger, "wrap, w 100!");
		controlPanel.add(saveGraph, "gaptop 20, w 100!");
		
		panelWithGraph.setLayout(new MigLayout());
		ChartPanel chartPanel = new ChartPanel(graph.chart);
		panelWithGraph.add(chartPanel, "span 2 2");
		panelWithGraph.add(controlPanel, "right");
	
		JPanel graphAndAnimation = new JPanel();
		graphAndAnimation.setLayout(new GridLayout(2,1,0,0));
		try {
			graphAndAnimation.add(new PanelWithAnimation(this));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		graphAndAnimation.add(panelWithGraph);
		
		add(graphAndAnimation, BorderLayout.CENTER);
		add(panelWithParametres, BorderLayout.WEST);
		
		//Kolory
		colorPink.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panelWithParametres.setBackground(Color.pink);
				controlPanel.setBackground(Color.getHSBColor(330, 34, 100));
				parametres.setBackground(Color.getHSBColor(330, 34, 100));
				voltageSlider.setBackground(Color.getHSBColor(330, 34, 100));
				knownWorkFunction.setBackground(Color.getHSBColor(330, 34, 100));
				undefinedWorkFunction.setBackground(Color.getHSBColor(330, 34, 100));
				panelWithGraph.setBackground(Color.getHSBColor(330, 34, 100));
			}
		});
		
		colorGray.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panelWithParametres.setBackground(null);
				controlPanel.setBackground(Color.GRAY);
				parametres.setBackground(null);
				voltageSlider.setBackground(null);
				knownWorkFunction.setBackground(null);
				undefinedWorkFunction.setBackground(null);
				panelWithGraph.setBackground(Color.GRAY);
			}
		});

		colorYellow.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panelWithParametres.setBackground(Color.getHSBColor(200, 34, 20));
				controlPanel.setBackground(Color.getHSBColor(200, 50, 20));
				parametres.setBackground(Color.getHSBColor(200, 50, 20));
				voltageSlider.setBackground(Color.getHSBColor(200, 50, 20));
				knownWorkFunction.setBackground(Color.getHSBColor(200, 50, 20));
				undefinedWorkFunction.setBackground(Color.getHSBColor(200, 50, 20));
				panelWithGraph.setBackground(Color.getHSBColor(200, 50, 20));
			}
		});
		
		colorGreen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panelWithParametres.setBackground(Color.getHSBColor(360, 300, 200));
				controlPanel.setBackground(Color.getHSBColor(360, 300, 100));
				parametres.setBackground(Color.getHSBColor(360, 300, 100));
				voltageSlider.setBackground(Color.getHSBColor(360, 300, 100));
				knownWorkFunction.setBackground(Color.getHSBColor(360, 300, 100));
				undefinedWorkFunction.setBackground(Color.getHSBColor(360, 300, 100));
				panelWithGraph.setBackground(Color.getHSBColor(360, 300, 100));
			}
		});
		
		colorBlue.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				panelWithParametres.setBackground(new Color( 79, 148, 205));
				controlPanel.setBackground(new Color(126, 192, 238));
				parametres.setBackground(new Color(126, 192, 238));
				voltageSlider.setBackground(new Color(126, 192, 238));
				knownWorkFunction.setBackground(new Color(126, 192, 238));
				undefinedWorkFunction.setBackground(new Color(126, 192, 238));
				panelWithGraph.setBackground(new Color(126, 192, 238));
			}
		});
		
	colorGray.doClick();	//Szary to domyślny kolor
	}
	
	
	/**
	 * Sprawdza, czy wpisana została liczba oraz czy mieści się w zadanym zakresie
	 * @param textField JTextField, do którego wpisana jest liczba
	 * @param min dolna granica przedziału
	 * @param max górna granica przedziału
	 * @return true jeżeli poprawnie wpisano liczbę, false w przeciwnym wypadku
	 */
	public boolean checkIntervall (JTextField textField, double min, double max){
		double numberFromJTextArea;
		try {
			numberFromJTextArea=Double.parseDouble(textField.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,stringToErrorMessage[1], "", 1);
			return false;
		}	
		if (numberFromJTextArea>=min && numberFromJTextArea<=max)
			return true;
		else {
			JOptionPane.showMessageDialog(null,stringToErrorMessage[0], "", 1);
			return false;
		}
	}
	

	/**
	 * Ustawia Look And Feel na Windows
	 */
	public void setLF(){
		try { 
			 for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) { 
				 if ("Windows".equals(info.getName())) { 
					 javax.swing.UIManager.setLookAndFeel(info.getClassName()); 
					 break;
				 }
			 }
		}catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex); 
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex); 
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex); 
		}		
	}

}