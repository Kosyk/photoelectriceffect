package pl.edu.pw.fizyka.pojava.GiedykKosyl;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.Timer;

/**
 * Klasa obliczeniowa. Przyjmuje wartości z klasy FirstInterface,
 * a przekazuje do Graph i PanelWithAnimation
 * @author Katarzyna Giedyk
 */
public class PhotoelectricCalculator  {
	final double h = 4.135667 * Math.pow(10, -15); // stała Plancka [eV*s]
	final double electronMass = 9.10938291 * Math.pow(10, -31);// masa elektronu [kg]															// [kg]
	final int speedOfLight = 299792458;// predkość światła[m/s]
	final double sector = 1;// droga [m]
	final double elementaryCharge = 1.602176565 * Math.pow(10, -19); // ładunek [C]
	
	volatile int wavelength;// długość fali [nm]
	volatile double workFunction;// praca wyjścia [eV]

	volatile double initialVelocity;// prędkość początkowa [m/s]
	volatile double acceleration;// przyspieszenie [m/s^2]
	volatile double photonEnergy; // początkowa energia kinetyczna fotonu [eV],
									// zmienna pomocnicza
	int particleAmount=1000;  //maksymalna ilosc elektronow
	volatile double distance[] = new double[particleAmount]; // aktualne położenie kulki
	volatile int deltaY[] = new int[particleAmount]; // przesuniecie y (losowane później)
	volatile double velocity[] = new double[particleAmount]; //predkosc kazdej kulki
	double deltaTime = 0.000000005; //krok czasowy dla obliczen
	int nForTimer=0; //zmienna pomocnicza, gwarantuje ze timer nie generuje za wielu kulek
	Timer simulationTimer=new Timer(20,null); // do wybijania kulek
	
	
	Map<Integer, Double> myMap = new HashMap<Integer, Double>(); //Do obsługi metali
	//alternatywa dla switch, case
	
	public PhotoelectricCalculator(Interface in){ // konstruktor
		setParametersFromInterface(in);
		for (int ii = 0; ii < particleAmount; ii++) {
			distance[ii] = -1; //-1 jest logicznym parametrem elektronu niewybitego
			deltaY[ii] = new Random().nextInt(60); //losowanie y
		}
		
		simulationTimer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(nForTimer==particleAmount) nForTimer = 0;
				//jezeli Timer doleciał do końca, to zaczyna od poczatku
				if(distance[nForTimer]==-1) distance[nForTimer]=0;
				//jezeli kulka jest niewybita, to ja wybija
				nForTimer++; //przejscie do nastepnej kulki
				
			}
		});
	}
	
	
	/**
	 * Ustawia parametry z interfesju
	 * @param in interfejs, z ktrórego pobierane są parametry
	 */
	public void setParametersFromInterface(Interface in){
		wavelength=Integer.parseInt(in.wavelengthField.getText());
		//dlugośc fali, zamiana tekstu z pola na int
		setWorkFunction(in); //zczytywanie pracy wyjscia
		
	}
	

	/**
	 * Ustawia pracę wyjścia, w zależności od scenariusza użycia aplikacji
	 * @param in interfejs, z którego pobierany jest aktualny scenariusz
	 */
	public void setWorkFunction(Interface in){
		myMap.put(0, 2.14);
		myMap.put(1, 2.75);
		myMap.put(2, 4.08);
		myMap.put(3, 4.5);
		myMap.put(4, 4.73);
		myMap.put(5, 5.01);
		myMap.put(6, 5.1);
		if (in.undefinedWorkFunctionField.isEnabled()){
			workFunction=Double.parseDouble(in.undefinedWorkFunctionField.getText());
		}
		else {
			int choose = in.metalList.getSelectedIndex();
			workFunction=myMap.get(choose);			
		}
	}
	
	/**
	 * Ustawia prędkość początkową fotonu.
	 * Sprawdza, czy foton ma wystarczającą energię kinetyczną, aby się wybić z płytki
	 */
	public void setInitialVelocity(){//prędkość wybijania
		photonEnergy = speedOfLight * h / wavelength * Math.pow(10, 9);//10^9
		if (photonEnergy > workFunction) {
			initialVelocity = Math.sqrt(2 * (photonEnergy - workFunction)
					* elementaryCharge / electronMass);
		} else {
			initialVelocity = 0;
		}
	}
	
	
	/**
	 * Ustawia aktualną prędkość dla danego elektronu
	 * @param argument indeks elektornu w tablicy
	 */
	public void setVelocity(int argument){
		velocity[argument]+=acceleration*deltaTime;
	}
	
	/**
	 * Ustawia przyspieszenie elektronu spowodowane napięciem
	 * @param v napięcie, z którego wyznaczane jest przyspieszenie
	 * @return przyspieszenie elektronu
	 */
	public double setAcceleration(double v) {
			if (initialVelocity == 0)
				return 0; //elektron jest nie wybity wiec nie liczymy dla niej 
			//przyspieszena
			else
				return v * elementaryCharge / electronMass / sector; //przyspieszenie
	}
	
	/**
	 * Wylicza położenie elektronu w zależności od czasu.
	 * @param particleNumber numer czątseczki, której dystans będzie wyliczany
	 */
	
	public void setDistance(int particleNumber) { //aktualny elektron
		if (distance[particleNumber] > -0.999) { //-1 oznacza elektron niewybity 
												//(coś jak parametr logiczny) 
			if (distance[particleNumber] >= 0
					&& distance[particleNumber] < sector) { //elektron jest między katodą, a anodą
				setVelocity(particleNumber);
				distance[particleNumber] += velocity[particleNumber]
						* deltaTime;
			} else { //dotarł do katody, albo anody
				velocity[particleNumber]=initialVelocity; // ustawia predkość początkową 
				deltaY[particleNumber] = new Random().nextInt(60);// ustawia przypadkowy y
				distance[particleNumber] = -1; // ustawia mu parametr niewybity
			}
		}
	}	
}
