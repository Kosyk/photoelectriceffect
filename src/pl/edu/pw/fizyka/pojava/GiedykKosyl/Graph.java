package pl.edu.pw.fizyka.pojava.GiedykKosyl;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


/**
 * Klasa reprezentująca wykres natężenia od napięcia
 * Wykres jest tworzony dla napięcia z zakresu <-5, 5> V
 * Jednostka natężenia - pA
 * @author Katarzyna Kosyl
 */
@SuppressWarnings("serial")
public class Graph extends JPanel{

		JFreeChart chart;
		XYSeries series;
		NumberAxis x,y;
		XYPlot plot;
		ChartPanel chartPanel;
		Interface refInterface;
		public Graph(Interface in) {
				refInterface = in;
				series = new XYSeries("XYGraph");	
				XYSeriesCollection dataset = new XYSeriesCollection();
				dataset.addSeries(series);
				chart=ChartFactory.createXYLineChart(
						null, "", "", dataset,
						PlotOrientation.VERTICAL, false, false, false);
				chartPanel = new ChartPanel(chart);
				add(chartPanel);
				NumberAxis x = new NumberAxis();
				x.setRange(-5, 5);
				x.setAttributedLabel("U [V]");
				NumberAxis y = new NumberAxis();
				y.setRange(-0.1, 2);
				y.setAttributedLabel("I [pA]");
				plot =(XYPlot) chart.getPlot();
				plot.setDomainAxis(x);
				plot.setRangeAxis(y);				
		}
		
		/**
		 * Wylicza i dodaje punkty do serii danych dla wykresu I[U]
		 * @param v aktualne napięcie
		 */
		public void calculateCurrent(double v){  
			double voltageToGraph=-7.0;
			double accelerationToGraph=0;
			double distanceToGraph=0;
			double timeToGraph=0;
			int check=0;
			double current=0;
			do {	
				accelerationToGraph=refInterface.photoelectricCalculator.setAcceleration(voltageToGraph);
				distanceToGraph=accelerationToGraph*timeToGraph*timeToGraph/2+refInterface.photoelectricCalculator.initialVelocity*timeToGraph;
				if (distanceToGraph>refInterface.photoelectricCalculator.sector)
					check++;
				timeToGraph+=0.0000001;
				current=check/timeToGraph/6.2415064/Math.pow(10, 18)*Math.pow(10, 12);//w pA
				voltageToGraph+=0.1;			
			} while (voltageToGraph<v);
			timeToGraph=0;
			series.add(v, current);
		}
}



