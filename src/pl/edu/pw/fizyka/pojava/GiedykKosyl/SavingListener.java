package pl.edu.pw.fizyka.pojava.GiedykKosyl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;

import javax.swing.JFileChooser;

import org.jfree.chart.ChartUtilities;
/**
 * Listener zapisujący wykres jako plik png i txt
 * @author Katarzyna Kosyl
 */
public class SavingListener implements ActionListener {

	Interface refInterface;
	public SavingListener(Interface in) {
		super();
		refInterface=in;
	}
	
	public void actionPerformed(ActionEvent arg0) {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Save");
		File currentFile;
		 @SuppressWarnings("rawtypes")
		List seriesList = refInterface.graph.series.getItems();
		int result = chooser.showDialog(null, "Save as txt...");
		if (JFileChooser.APPROVE_OPTION == result){ 
		    if (result != JFileChooser.APPROVE_OPTION){
		    return;
		    }
		    try{
				currentFile=chooser.getSelectedFile();
				String fileName = chooser.getSelectedFile().getAbsolutePath();
		        if(!fileName.endsWith(".txt")){
		            fileName+=".txt";
		            currentFile=new File(fileName);
		        }
			    BufferedWriter bufferedWriter = new BufferedWriter(
			    	new OutputStreamWriter(
			    			new FileOutputStream(currentFile), Charset.forName("UTF-8")
			    	)
			   	);
			    bufferedWriter.write("W:"+refInterface.photoelectricCalculator.workFunction+"[V]\tλ:"
			    		+refInterface.photoelectricCalculator.wavelength+"[nm]");
			    bufferedWriter.newLine();
			    bufferedWriter.write("U[V]:\tI[pF]:");
			    bufferedWriter.newLine();
			    for (int ii=0; ii<seriesList.size(); ii++){
			    	bufferedWriter.write(""+seriesList.get(ii));
			    	bufferedWriter.newLine();
			    }
			    bufferedWriter.close();
		    }catch(IOException e1){
		    e1.printStackTrace();
		    }
		  }
		  else {
		    System.out.println("File not chosen");
		  }
		  result = chooser.showDialog(null, "Save as png..."); 
		  if (JFileChooser.APPROVE_OPTION == result){ 
			    if (result != JFileChooser.APPROVE_OPTION){
			    return;
			    }
				try {
					currentFile=chooser.getSelectedFile();
					String fileName = chooser.getSelectedFile().getAbsolutePath();
			        if(!fileName.endsWith(".png")){
			            fileName+=".png";
			            currentFile=new File(fileName);
			        }
					ChartUtilities.saveChartAsPNG(currentFile, refInterface.graph.chart, 800, 400);
				} catch (IOException e) {
					e.printStackTrace();
				}
			  }
			  else {
			    System.out.println("File not chosen");
			  }
	}
}

