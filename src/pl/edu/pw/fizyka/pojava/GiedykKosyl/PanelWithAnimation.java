package pl.edu.pw.fizyka.pojava.GiedykKosyl;
import com.kitfox.svg.SVGDiagram;
import com.kitfox.svg.SVGDisplayPanel;
import com.kitfox.svg.SVGException;
import com.kitfox.svg.SVGUniverse;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.net.URI;

@SuppressWarnings("serial")
/**
 * Zawiera schemat układu oraz animacje
 */
public class PanelWithAnimation extends SVGDisplayPanel { 

    SVGDiagram svgDiagram; //obrazek
    Interface refInterface; 
    public PanelWithAnimation(Interface in) throws Exception{ 
    	// konstruktor przyjmujacy referencje
    	refInterface=in;
        SVGUniverse svgUniverse = new SVGUniverse();
        URI uri = svgUniverse.loadSVG(getClass().getResourceAsStream("Picture1.svg"), "Rysunek");
        svgDiagram = svgUniverse.getDiagram(uri);
        //przygotowanie obrazka
        
    }

    public void paintComponent(Graphics g) {
    	//schemat
        super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
        try {
        	AffineTransform at = new AffineTransform();
        	at.setToScale(this.getHeight()/svgDiagram.getHeight(), this.getHeight()/svgDiagram.getHeight());
        	g2d.transform(at);
            svgDiagram.render(g2d);
        } catch (SVGException e) {
            e.printStackTrace();
        } //obsługa skalowania obrazka, rzuca wyjatki dla bledów
        
        //animacja, wyswietlanie kulek
        double simulaionBorders = 404; //skalowanie modelu fizycznego na piksele
        g2d.setColor(Color.BLUE);
        for (int ii = 0; ii < refInterface.photoelectricCalculator.particleAmount; ii++) {
        	if (refInterface.photoelectricCalculator.distance[ii] > 0.001	&&
            		refInterface.photoelectricCalculator.distance[ii] < (refInterface.photoelectricCalculator.sector))
            {
            	g2d.fillOval((int) (refInterface.photoelectricCalculator.distance[ii]*simulaionBorders)+100,
            			140+refInterface.photoelectricCalculator.deltaY[ii], 8, 8);
            }
        }
    }
    
}
