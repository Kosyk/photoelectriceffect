package pl.edu.pw.fizyka.pojava.GiedykKosyl;
import java.awt.GridLayout;
import javax.swing.JApplet;

/**
 * @author Katarzyna Giedyk
 */


@SuppressWarnings("serial")
public class PhotoelectricApplet extends JApplet {

	volatile boolean apletStopped; //informuje czy aplet jest zatrzymany
	volatile boolean panelIsCreated = false; //informuje czy główny panel już istnieje

	volatile Interface panelWithInterface;

	public void init() { //włącza się przy starcie programu
		setSize(840, 600);
		setLayout(new GridLayout());
		setVisible(true);
		if (!panelIsCreated) {
			panelWithInterface = new Interface();
			add(panelWithInterface);
			panelWithInterface.backgroundThread.start(); //startuje drugi watek
			panelIsCreated = true;
			panelWithInterface.setVisible(true);
		}
		// 3 linie "śmieci", żeby animacja działa
		//dobrze przy kazdej konfiguracji włączania, minimalizowania itp
		panelWithInterface.ifSimulate = true; 
		panelWithInterface.simulationTrigger.doClick();
		apletStopped = false;
	}

	public void start() {

		if (apletStopped) {

			panelWithInterface.ifSimulate = true;
			panelWithInterface.photoelectricCalculator.simulationTimer.start();
			apletStopped = false;
		}

	}

	public void stop() {
		if (panelWithInterface.ifSimulate) {
			panelWithInterface.ifSimulate = false;
			panelWithInterface.photoelectricCalculator.simulationTimer.stop();
			apletStopped = true;
		}
	}

}
