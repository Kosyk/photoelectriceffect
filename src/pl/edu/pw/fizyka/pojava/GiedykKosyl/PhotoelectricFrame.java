package pl.edu.pw.fizyka.pojava.GiedykKosyl;

import java.awt.Dimension;
import java.awt.HeadlessException;

import javax.swing.JFrame;
/**
 * Klasa otwierająca interface jako ramkę. 
 * @author Katarzyna Kosyl 
 */
@SuppressWarnings("serial")
public class PhotoelectricFrame extends JFrame {

	Interface frame;
	public PhotoelectricFrame() throws HeadlessException {
		setMinimumSize(new Dimension(840, 620));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame = new Interface();
		add(frame);
		setVisible(true);
	}

	public static void main(String[] args) {
		PhotoelectricFrame photoelectricFrame = new PhotoelectricFrame();
		photoelectricFrame.frame.backgroundThread.start();
	}
	
}
